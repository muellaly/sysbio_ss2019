{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 6 – ODEs, Two reaction model\n",
    "\n",
    "_**The goal of this exercise is to formulate and implement a simple kinetic model of glycolysis.**_\n",
    "\n",
    "In the figure below, on the left, you can see a detailed depiction of the glycolysis pathway with all intermediate steps. On the right, you can see a simplified model that only includes two metabolites (glucose-6-phosphate from upper glycolysis, phosphoenolpyruvate from lower glycolysis) and three reactions. In the following exercise, you will implement this simplified model and explore it using the PySB toolbox. We will refer to the metabolites and reactions according to the abbreviations shown in the figure (constant carbon influx influx, enzyme reactions _gly_ and _pyk_ and metabolites G6P and PEP).\n",
    "\n",
    "<img src='glycolysis.png'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem 1: Set-up of a two reaction model (pen & paper)\n",
    "In order to implement your model in Matlab, you first have to identify the kinetic equations underlying the system. \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_a) Based on the reaction scheme of the simplified model of glycolysis, write the Balance Equations for the species’ concentrations (G6P, PEP) under the assumption of Michaelis Menten kinetics and a constant Carbon Influx._"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\n",
    "\\frac{d[G6P]}{dt} = ...\\\\\n",
    "\\frac{d[PEP]}{dt} = ...\n",
    "$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_b) Find the concentrations of species G6P and PEP in steady state, using the parameters from the table below._\n",
    "\n",
    "Hint: In steady state, $\\frac{d[G6P]}{dt} = 0$ and $\\frac{d[PEP]}{dt} = 0$ \n",
    "\n",
    "|Parameter Name|Value|Unit|\n",
    "|--------------|-----|----|\n",
    "|$CarbonInflux$  | 2   |mM/sec|\n",
    "|$K_M GLY$|0.16|mM|\n",
    "|$V_{max} GLY$|5|mM/sec|\n",
    "|$K_M PYK$|0.31|mM|\n",
    "|$V_{max} PYK$|5|mM/sec|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "???"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem 2: Implementing and simulating the model (PySB)\n",
    "In the Python file `small_glycolysis.py` the simplified glycolysis model is already defined with the appropriate kinetic laws.\n",
    "Open the model in the jupyter browser by clicking the `small_glycolysis.py` file. In order to run a simulation of your model, you first have to adjust all the parameters and initial concentrations:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "a) In your model, define the ‘Initial’ amount of G6P and PEP and enter the parameters from the table above.\n",
    "\n",
    "`Parameters(...)`\n",
    "\n",
    "`Initial` amount G6P_0 [mM]: 3\n",
    "\n",
    "`Initial` amount PEP_0 [mM]: 0.5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, your model is ready for simulation. First, you would like to know whether your results for the steady state values in Problem I b were correct. \n",
    "    b) Simulate the dynamic behavior of the system for 10 seconds. Do the steady state values of G6P and PEP agree with your calculations?\n",
    "Hint: To simulate In the SimBiology window, click ‘Add Task’ -> ‘Simulate Model’, click ‘Run’\n",
    "Hint: You can select for how long the system should be simulated: In the simulation window, click on ‘Simulation Settings’ and change the value in the field ‘StopTime’ to a new value (e.g. 10 for 10 s).\n",
    "\n",
    "We want to explore how robust our model is. First, we test how different initial concentrations of G6P and PEP influence their steady state concentrations:\n",
    "    c) Change the initial amounts of the species concentrations to the values given in the table below. For every pair of initial concentrations, simulate the model and inspect the concentration changes. Do the steady state values of the species change? How do you interpret the results?\n",
    "#\n",
    "Species Name\n",
    "Initial Value [mM]\n",
    "1\n",
    "G6P\n",
    "0.03\n",
    "\n",
    "PEP\n",
    "0.005\n",
    "2\n",
    "G6P\n",
    "0.01\n",
    "\n",
    "PEP\n",
    "7\n",
    "3\n",
    "G6P\n",
    "10\n",
    "\n",
    "PEP\n",
    "0.2\n",
    "\n",
    "In order to analyze how the KM and vmax parameters of the reactions gly and pyk influence the system behavior, we will perform simulations at different values of each parameter:\n",
    "    d) Scan kmGLY and vmaxPYK in the ranges detailed below. How do the parameters influence the steady state values of G6P and PEP?\n",
    "\n",
    "\n",
    "\n",
    "Hints:\n",
    "    • First, change the InitialAmount of G6P and PEP back to their initial values (3 and 0.5).\n",
    "    • In the SimBiology toolbox, click ‘Add Task’ -> ‘Simulate’\n",
    "        ◦ On the left, in the section ‘Adjust Quantities’ press ‘Options’\n",
    "        ◦ Tick the parameter name you want to scan (e.g. kmGLY)\n",
    "        ◦ Set the ‘Min’ and ‘Max’ to the minimum and maximum values you want to (see below)\n",
    "        ◦ Press ‘OK’\n",
    "    • Click ‘Run’\n",
    "Now, you will see the simulated curves for G6P and PEP. With the slider that appeared on the left, you can explore how changes in the selected parameter affect the steady state values of your system.\n",
    "\n",
    "Problem 3: Extending the model: feedback inhibition (SimBiology)\n",
    "For the last part of this exercise, we will explore how a feedback inhibition loop influences the dynamic behavior and steady state values of our model:\n",
    "\n",
    "First, we have to implement this feedback inhibition in our SimBiology model. To do so, save the current model to a new file ‘SmallGlycolysis_wInh.sbproj’. The non-competitive (allosteric) feedback inhibition of metabolite PEP on the reaction gly affects the maximum reaction rate of the enzyme depending on the current concentration of PEP ().\n",
    "    a) Include the regulation in your new model: Multiply the kinetic law with a power law term, where  is a parameter describing the strength of the inhibition. For our simulations, we will set a to ‘-2’.\n",
    "Hints\n",
    "    • Save the current model to a new file ‘SmallGlycolysis_wInh.sbproj’.\n",
    "    • Drag and drop a new parameter into the ‘cell’ compartment and name it ‘a’, set the initial amount to -2 \n",
    "    • Click on the reaction ‘glycolysis’ and change the Kinetic law to ‘Unknown’\n",
    "    • Edit the reaction rate: vmaxGLY*G6P*(PEP^a)/(kmGLY+G6P)\n",
    "    • Save the model again\n",
    "If you have problems with the implementation, you can use the model ‘SmallGlycolysis_wInh.sbproj’ that is provided in your exercise folder.\n",
    "\n",
    "    b) How does the feedback loop affect the steady state levels of the metabolites? Simulate the new model to answer this question. How do you interpret the results?\n",
    "Now, we want to compare how the model with and without feedback inhibition respond to perturbations in the environment. We will analyze the effects of a step-like increase of the influx and of a pulse-like increase of the influx on the steady state values of both metabolites.\n",
    "\n",
    "    c) Using both of your models (with and without inhibition) add a step-like increase (to 5 mM/s) of the carbon influx after 40 seconds and plot the levels of both metabolites in the first 200 s.\n",
    "Hint: Drag and drop an ‘event’ from the panel on the left into the ‘cell’ compartment and set ‘if time >=’ to 40, in the field below enter ‘CarbonInflux = 5’. Remember that you can change the simulation time (see above problem 2-b).\n",
    "\tHow do you interpret the results?\n",
    "\n",
    "\n",
    "    d) Compare the behavior of both models in case of a 40 second pulse-like increase of the input. Include the pulse-like increase of the input and simulate both systems for 200 seconds. \n",
    "Hint: To achieve a pulse-like input increase that happens after 40 seconds in simulation and terminates after 80 seconds, add a second event to your model before simulating the system: Drag and drop an ‘event’ from the panel on the left into the ‘cell’ compartment and set ‘if time >=’ to 80, in the field below enter ‘CarbonInflux = 2’.\n",
    "Why would a biological system need to develop such a regulation mechanism?\n",
    "\n",
    "\n",
    "OPTIONAL: Further model exploration\n",
    "If you are interested in further exploring the model and the SimBiology toolbox, here are some ideas on what to investigate:\n",
    "Exploring additional types of regulation\n",
    "Starting from the model ‘SmallGlycolysis.sbproj’ add other types of regulatory interactions (e.g. uncompetitive inhibition, noncompetitive inhibition, activation) and explore their effect on the system behavior.\n",
    "How many different types of dynamic profiles can you implement?\n",
    "Exploring combinations of parameters\n",
    "Starting from the model ‘SmallGlycolysis.sbproj’ with or without additional regulation, explore how different parameters influence the system behavior. For example, you can define ‘sliders’ for all parameters you are interested in and investigate multiple parameters at once.\n",
    "Are the metabolite levels particularly sensitive/robust to changes in certain parameters? "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
