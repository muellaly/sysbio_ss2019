{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 12: Molecular Classification of Cancer (part I)\n",
    "\n",
    "One of the challenges in cancer treatment is correct identification of the type of cancer in order to apply the most suitable treatment. We would like to test the feasibility of using gene expression data obtained by DNA microarrays to classify cancer. Our focus is on two types of leukemia: acute myeloid leukemia (AML) and acute lymphoblastic leukemia (ALL).\n",
    "\n",
    "Our initial leukemia data set consisted of 38 bone marrow samples (27 ALL, 11 AML) obtained from acute leukemia patients at the time of diagnosis. The file `ex12.npz` contains three variables:\n",
    "- `expression` - a 2D array containing the log2-scale data from the DNA microarrays (shape = 7129x38)\n",
    "- `genes` - a 1D array with the gene names\n",
    "- `true_labels` - a 1D array indicating the types of leukemia"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# imports\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import scipy.stats\n",
    "import sklearn.decomposition\n",
    "import sklearn.cluster\n",
    "\n",
    "# importing the data and utility scripts from the previous exercise\n",
    "import sys\n",
    "sys.path.append(\"../09_Univariate_Analysis\")\n",
    "import ex9_util"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualizing high dimensional data\n",
    "Load the data from `ex12.npz` and set the random seed to `np.random.seed(1982)`. Do not run this command before every `kmeans()` command, but rather *only once* at the top of your script."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Univariate analysis\n",
    "Start by performing the standard analysis using `scipy.stats.ttest_ind()`, `ex9_util.qvalue()`, and `ex9_util.volcano()`. Use the volcano plot to find out which up regulated gene has the most significant $q$-value and the same for the down regulated genes. Use fold-change threshold of 4.\n",
    "\n",
    "* Hint: Use `dataALL = data['expression'][:, data['true_labels'] == 'ALL']` and `dataAML = data['expression'][:, data['true_labels'] == 'AML']` to define the two groups of samples.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Scatter plot using 2 genes\n",
    "Find the index of the two genes from the previous questions. Plot the expression of these two genes using a scatter plot and use `data['true_labels']` for the colors.\n",
    "\n",
    "* Hint: Use `ax.scatter(x, y, label= ... )` to make the plot, where `x` and `y` are the expression levels of the two genes from quesion 1.1 across all 37 patients, only for the patients with one of the cancer types (you can use `dataALL` and `dataAML` from the previous question)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#figure = plt.figure(figsize=(6, 4))\n",
    "#ax = figure.add_subplot(1, 1, 1)\n",
    "#\n",
    "# <FILL IN YOUR CODE HERE>\n",
    "#\n",
    "#ax.set_xlabel( ... )\n",
    "#ax.set_ylabel( ... )\n",
    "#ax.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3 Use $k$-means to see if it manages to distinguish the two clusters automatically\n",
    "Using only the expression of the two genes you found in 1.1, run [`sklearn.cluster.KMeans()`](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html) and make another scatter plot with the resulting cluster labels. Do you spot any differences between the true labels and the clusters? How many samples were misclassified?\n",
    "\n",
    "* Remember that $k$-means assumes that the rows are samples and the columns are feature dimensions. Therefore, the data from the `data['expression']` matrix needs to be transposed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.4 Dimension reduction using PCA\n",
    "Reduce the dimension of the data in `data['expression']` using PCA to 2, and present it in a scatter plot. Use the `true_labels` to color the points.\n",
    "\n",
    "* Hint:\n",
    "To run PCA on a matrix `X` and project the data on the 2 first principal components, use [`sklearn.decomposition.PCA()`](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html):\n",
    "```\n",
    "pca = sklearn.decomposition.PCA(n_components=2)\n",
    "projected_data = pca.fit_transform(X)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.5 Use $k$-means to see if it manages to distinguish the two clusters automatically\n",
    "Repeat what you did in Question 1.3, but instead of using two specific genes -- use the projected data on the two principal components. Does $k$-means perform better now? How many misclassified samples do you observe this time?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.6 Select only highly significantly changing genes\n",
    "Now, combine the two approaches of selecting genes by $p$-value and PCA for dimension reduction. First, find the indices of those genes whose (FDR-corrected) $p$-value is lower than 0.001. Then, select only these rows from the `data['expression']` matrix and perform a PCA projection into 2D (same as in question 1.4). Again, plot the result in a scatter plot and use the true labels for colors.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.7 Use $k$-means to see if it manages to distinguish the two clusters automatically\n",
    "Once again, use $k$-means on the newly projected values and plot the result in a scatter plot. Does $k$-means perform better now? How many misclassified samples do you observe this time?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
